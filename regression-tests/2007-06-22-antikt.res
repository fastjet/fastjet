
FASTJET regression tests on Fri Jun 22 15:52:08 CEST 2007

Reference exec: ../example/fastjet_timing_plugins
Test exec:      ../example/fastjet_timing_plugins
Data file:      /ada1/lpthe/salam/work/fastjet/data/Pythia-PtMin50-LHC-1000ev.dat
Nev used:       1000
Alg list:       antikt
Filter:         grep -v -e '^#' -e 'strategy' -e '^Algorithm:'


******* Running reference result for antikt (-antikt)
Running strategy -04 for antikt
Number of differences wrt ref = 0
Running strategy -03 for antikt
Number of differences wrt ref = 0
Running strategy -02 for antikt
Number of differences wrt ref = 0
Running strategy -01 for antikt
Number of differences wrt ref = 0
Running strategy +02 for antikt
Number of differences wrt ref = 0
Running strategy +03 for antikt
Number of differences wrt ref = 0
Running strategy +04 for antikt
Number of differences wrt ref = 0
Running strategy +00 for antikt
Number of differences wrt ref = 0
