
FASTJET regression tests on Fri Jun 22 16:22:23 CEST 2007

Reference exec: /ada1/lpthe/salam/tmp/fastjet-2.1.0/example/fastjet_timing_plugins
Test exec:      ../example/fastjet_timing_plugins
Data file:      /ada1/lpthe/salam/work/fastjet/data/Pythia-PtMin50-LHC-1000ev.dat
Nev used:       1000
Alg list:       siscone1
Filter:         grep -v -e '^#' -e 'strategy' -e '^Algorithm:'


******* Running reference result for siscone1 (-siscone -npass 1)
Running strategy 00 for siscone1
Number of differences wrt ref = 0
